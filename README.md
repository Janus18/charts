# Charts

Some Helm charts for self-hosted services

## Pre-requisites
* Cluster is expected to be configured with IP 10.0.0.10. For this, I used keepalived as described [here](https://www.redhat.com/en/blog/keepalived-basics).
* Secrets are handled using [sops](https://github.com/getsops/sops) and [helm secrets](https://github.com/jkroepke/helm-secrets/wiki). There are two keys:
    * One [age](https://github.com/FiloSottile/age) key which must be stored in the host where `helm secrets` is executed. This key is also used within Argo to decode the secrets.
    * And one [kms](https://docs.aws.amazon.com/kms/latest/developerguide/overview.html) backup key, for which `aws` CLI tool is needed.
* There is a NAS the containers can connect to, which should be named **qnap.aemona.top**. This value can be changed in the *values* files of the different projects which are using this server. To get all of them, using *grep* should be fine.

## Installing
Argo project was modified to use `helm secrets` instead of plain helm, so it is possible to bootstrap everything by installing just Argo via `helm secrets`, then use Argo itself to create all other projects:
```
cd argo && helm secrets template -f argo-values-override.yaml -f argo-secrets-override-enc.yaml -f values.yaml -f secrets-enc.yaml -n argocd argo-cd .
```

*This method of installation hasn't been tested as Argo project was added after other projects were*.

Other core projects should be installed before any others. If these are not installed, the projects will be created anyway but the containers will keep failing until dependencies are there. Core projects: *cert-manager* and *longhorn*.