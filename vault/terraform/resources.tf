resource "aws_kms_key" "vault" {
  description = "Vault unseal key"
  tags        = local.common_tags
}

resource "aws_kms_alias" "vault-alias" {
  name          = "alias/vault-key"
  target_key_id = aws_kms_key.vault.key_id
}

resource "aws_iam_access_key" "vault-user-key" {
  user = aws_iam_user.vault-user.name
}

resource "aws_iam_user" "vault-user" {
  name = "vault-user"
  tags = local.common_tags
}

resource "aws_iam_user_policy" "vault-user-policy" {
  name   = "vault-user-policy"
  user   = aws_iam_user.vault-user.name
  policy = aws_iam_policy.policy.policy
}

resource "aws_iam_policy" "policy" {
  name        = "vault-user-key-policy"
  path        = "/"
  description = "Gives access to Vault key in KMS"
  tags = local.common_tags

  policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = [
            "kms:DescribeKey",
				    "kms:Decrypt",
				    "kms:Encrypt"
        ]
        Effect   = "Allow"
        Resource = aws_kms_key.vault.arn
      },
    ]
  })
}
