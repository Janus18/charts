output "user-key-id" {
  sensitive = true
  value     = {
    id = aws_iam_access_key.vault-user-key.id
    secret = aws_iam_access_key.vault-user-key.secret
  }
}
