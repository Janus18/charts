{{- define "proton-bridge.name" -}}
{{- if .Values.name -}}
{{- .Values.name | trunc 63 | trimSuffix "-" -}}
{{- else -}}
{{- .Chart.Name | trunc 63 | trimSuffix "-" -}}
{{- end -}}
{{- end -}}

{{- define "proton-bridge.shortname" -}}
{{- if .Values.name -}}
{{- .Values.name | trunc 50 | trimSuffix "-" -}}
{{- else -}}
{{- .Chart.Name | trunc 50 | trimSuffix "-" -}}
{{- end -}}
{{- end -}}

