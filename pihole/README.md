## Deploy

Assumes a Storage Class named `longhorn` exists.
webpassword and localip should be set when installing the chart.

* Install the chart  
  ```helm install --set 'webpassword=<password>,localip=<localip>' <release> pihole```

