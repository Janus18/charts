{{- define "image-updater.name" -}}
argocd-image-updater
{{- end -}}

{{- define "common.labels" -}}
helm.sh/chart: {{ .Chart.Name }}-{{ .Chart.Version | replace "+" "_" }}
app.kubernetes.io/version: {{ .Chart.AppVersion | quote }}
app.kubernetes.io/instance: {{ .Release.Name }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
app.kubernetes.io/part-of: {{ include "image-updater.name" . }}
{{- end -}}

{{- define "image-updater.labels" -}}
app.kubernetes.io/name: {{ include "image-updater.name" . }}-$NAME
{{ include "common.labels" . }}
{{- end -}}

