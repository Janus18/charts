output "key-arn" {
  value     = {
    secret = aws_kms_key.helm_secrets.arn
  }
}
