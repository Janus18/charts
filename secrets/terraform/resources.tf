resource "aws_kms_key" "helm_secrets" {
  description             = "To be used for Helm Secrets"
  deletion_window_in_days = 14
  enable_key_rotation     = true
  tags                    = local.common_tags
}

resource "aws_kms_alias" "helm_secrets" {
  name          = "alias/helm-secrets"
  target_key_id = aws_kms_key.helm_secrets.key_id
}
